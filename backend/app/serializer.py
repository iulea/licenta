from collections import OrderedDict

from rest_framework import serializers

from app.models import Destination, User, Review


class DestinationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Destination
        fields = ('id', 'name', 'country', 'city', 'category', 'stars')


class UserSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = User
        fields = ('id', 'username', 'email', 'firstName', 'lastName', 'password')
        extra_kwargs = {'password': {'write_only': True}}


class UserField(serializers.PrimaryKeyRelatedField):

    def to_representation(self, value):
        id = super(UserField, self).to_representation(value)
        try:
          user = User.objects.get(pk=id)
          serializer = UserSerializer(user)
          return serializer.data
        except User.DoesNotExist:
            return None

    def get_choices(self, cutoff=None):
        queryset = self.get_queryset()
        if queryset is None:
            return {}

        return OrderedDict([(item.id, self.display_value(item)) for item in queryset])


class ReviewSerializer(serializers.ModelSerializer):
    user = UserField(queryset=User.objects.all())

    class Meta:
        model = Review
        fields = ('id', 'destination', 'user', 'cost', 'time', 'family', 'interactive', 'newppl', 'loud', 'overall', 'comment', 'dateAdded', 'considered', 'user')
