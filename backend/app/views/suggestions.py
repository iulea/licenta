from django.db.models import Avg
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from app.models import Review, Destination
from app.serializer import DestinationSerializer

from queue import PriorityQueue


class SuggestionsView(viewsets.ModelViewSet):
    serializer_class = DestinationSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user.id
        if len(self.request.query_params) == 1:
            country = self.request.query_params.get('country', None)
        else:
            country = None
        userReviews = Review.objects.all().filter(user=user).filter(considered=True)
        if country:
            allDestinations = Destination.objects.all().filter(country=country).filter(approved=1)
        else:
            allDestinations = Destination.objects.all().filter(approved=1)

        priorityQueue = PriorityQueue()

        userAvgCost = userReviews.aggregate(Avg('cost'))
        userAvgTime = userReviews.aggregate(Avg('time'))
        userAvgFamily = userReviews.aggregate(Avg('family'))
        userAvgInteractive = userReviews.aggregate(Avg('interactive'))
        userAvgNewppl = userReviews.aggregate(Avg('newppl'))
        userAvgLoud = userReviews.aggregate(Avg('loud'))

        for dest in allDestinations:
            revs = Review.objects.all().filter(destination=dest.id).exclude(user=user)
            if revs and user and userReviews:
                avgCost = revs.aggregate(Avg('cost'))
                avgTime = revs.aggregate(Avg('time'))
                avgFamily = revs.aggregate(Avg('family'))
                avgInteractive = revs.aggregate(Avg('interactive'))
                avgNewppl = revs.aggregate(Avg('newppl'))
                avgLoud = revs.aggregate(Avg('loud'))

                sum = abs(userAvgCost['cost__avg'] - avgCost['cost__avg']) + abs(
                    userAvgTime['time__avg'] - avgTime['time__avg']) + abs(
                    userAvgFamily['family__avg'] - avgFamily['family__avg']) + abs(
                    userAvgInteractive['interactive__avg'] - avgInteractive['interactive__avg']) + abs(
                    userAvgNewppl['newppl__avg'] - avgNewppl['newppl__avg']) + abs(
                    userAvgLoud['loud__avg'] - avgLoud['loud__avg'])

                if sum < 5:
                    priorityQueue.put((sum, dest))

        result = []
        while not priorityQueue.empty():
            next_item = priorityQueue.get()
            result.append(next_item[1])
        return result
