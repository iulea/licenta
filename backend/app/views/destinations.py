from rest_framework import viewsets

from app.models import Destination
from app.serializer import DestinationSerializer


class DestinationsView(viewsets.ModelViewSet):
    serializer_class = DestinationSerializer

    def get_queryset(self):
        if len(self.request.query_params) == 1:
            arg = self.request.query_params.get('country', None)
            return Destination.objects.select_related('category').all().filter(country=arg).filter(approved=1).order_by('-stars')
        else:
            return Destination.objects.all().order_by('-stars').filter(approved=1)
