from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from app.models import Review
from app.serializer import ReviewSerializer


class ReviewsView(viewsets.ModelViewSet):
    serializer_class = ReviewSerializer

    def get_queryset(self):
        if len(self.request.query_params) == 1:
            arg = self.request.query_params.get('destination', None)
            return Review.objects.select_related('user').filter(destination=arg).order_by('-dateAdded')
        else:
            return Review.objects.select_related('user').all().order_by('-dateAdded')


class CreateReviewView(APIView):

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        review = request.data
        user = request.user.id
        review['user'] = user
        serializer = ReviewSerializer(data=review)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        print(review)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
