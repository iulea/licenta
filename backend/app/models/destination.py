from django.db import models

from app.models.category import Category


class Destination(models.Model):
    name = models.CharField(max_length=200, blank=False)
    country = models.CharField(max_length=50, blank=False)
    city = models.CharField(max_length=50, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=False)
    approved = models.BooleanField(default=0)
    stars = models.FloatField(default=0)
