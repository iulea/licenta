from datetime import date

from django.db import models

from app.models.user import User
from app.models.destination import Destination


class Review(models.Model):
    destination = models.ForeignKey(Destination, on_delete=models.CASCADE, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    cost = models.IntegerField()
    time = models.IntegerField()
    family = models.IntegerField()
    interactive = models.IntegerField()
    newppl = models.IntegerField()
    loud = models.IntegerField()
    overall = models.IntegerField(default=5)
    comment = models.TextField(default="")
    dateAdded = models.DateField(default=date.today)
    considered = models.BooleanField(default=True)
