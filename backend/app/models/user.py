from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin
)

from app.models.userManager import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    """
    username = models.CharField(max_length=50, blank=False, unique=True)
    password = models.CharField(max_length=200, blank=False)
    firstName = models.CharField(max_length=50, blank=False)
    lastName = models.CharField(max_length=50, blank=False)
    email = models.CharField(max_length=50, blank=False, unique=True)
    isAdmin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'firstName', 'lastName']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self
