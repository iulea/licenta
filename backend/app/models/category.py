from django.db import models


class Category(models.Model):
    categoryName = models.CharField(max_length=50, blank=False)
