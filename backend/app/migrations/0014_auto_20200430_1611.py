# Generated by Django 2.1.7 on 2020-04-30 13:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_destination_stars'),
    ]

    operations = [
        migrations.AlterField(
            model_name='destination',
            name='stars',
            field=models.FloatField(default=0),
        ),
    ]
