import {Route, Switch, Redirect} from "react-router-dom";
import React from "react";
import Home from "./components/dumb/pages/Home";
import Login from "./components/dumb/pages/Login";
import SignUp from "./components/dumb/pages/SignUp";
import DestinationsList from './components/dumb/pages/DestinationsList';
import DestinationPage from './components/dumb/pages/DestinationPage';
import Review from './components/dumb/pages/Review';
import AddDestination from './components/dumb/pages/AddDestination';


const RoutingBasicComponent = (props) => {


        return (
            <React.Fragment>
                <Switch>
                    {localStorage.getItem("Access-Token") ? null : <Route exact path='/login' render={() => <Login/>}/>}
                    {localStorage.getItem("Access-Token") ? null : <Route exact path='/signup' render={() => <SignUp/>}/>}
                    {localStorage.getItem("Access-Token") ? <Route exact path='/review/:id' component ={Review}/> : null}
                    {localStorage.getItem("Access-Token") ? <Route exact path='/addDestination' render={() => <AddDestination/>}/> : null}
                    <Route exact path='/destinations/:country' component={DestinationsList}/>
                    <Route exact path='/destination/:id' component={DestinationPage}/>
                    {<Route exact path='/' render={() => <Home/>}/>}
                    <Redirect to='/'/>
                </Switch>
            </React.Fragment>
        );

};

export default RoutingBasicComponent
