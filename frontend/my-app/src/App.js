import React, { Component } from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import RoutingBasicComponent from "./RoutingComponent";

class App extends Component {

  constructor(props)
  {
    super(props);
  }

  render()
  {
    return (
      <div className="App">
        <BrowserRouter>
          <div className='App'>
            <div className='Home'>
              <RoutingBasicComponent/>
            </div>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
