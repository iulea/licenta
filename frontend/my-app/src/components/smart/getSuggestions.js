import {action, extendObservable} from "mobx";

class storeSuggestions {
    constructor(){
        extendObservable(this, {
            destinations: [],
            loaded: false,
            user: null,
            country: null,
            loadDestinations: action(function() {
                fetch(this.country ? `http://127.0.0.1:8000/api/suggestions/?country=` + this.country : `http://127.0.0.1:8000/api/suggestions/`,{
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + localStorage.getItem('Access-Token')
                    },
                })
            .then(data => data.json())
            .then(response => {
                this.destinations = response;
                this.country = null;
            })
            }),
            unload: action(function() {
                this.destinations = []
            })
        })
    }

}

const store = new storeSuggestions();
export default store;