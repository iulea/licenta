import {action, extendObservable} from "mobx";

class storeDestinations {
    constructor(){
        extendObservable(this, {
            destinations: [],
            loaded: false,
            country: null,
            loadDestinations: action(function() {
                fetch(this.country ? `http://127.0.0.1:8000/api/destinations/?country=` + this.country : `http://127.0.0.1:8000/api/destinations/`)
            .then(data => data.json())
            .then(response => {
                this.destinations = response;
                this.country = null;
            })
            }),
            unload: action(function() {
                this.destinations = []
            })
        })
    }

}

const store = new storeDestinations();
export default store;