import React from 'react';
import '../../styles/all.css';
import Rating from '@material-ui/lab/Rating';
import { Link } from "react-router-dom";

class Destination extends React.Component{
    constructor(props) {
        super(props);
      
        this.state = {
          name: props.name,
          id: props.id,
          country: props.country,
          city: props.city,
          category: props.category,
          stars: props.stars
        };
      }

      render() {
        return <Link to={"/destination/" + this.state.id}>
        <div className="destination">
            <h3>{this.state.name}</h3>
            <div className="destinationDetails">
              <span>{this.state.country}: <strong>{this.state.city}</strong></span>
              <Rating name="half-rating" value={this.state.stars} precision={0.1} readOnly/>
            </div>
        </div>
        </Link> 
      }
}

export default Destination