import React, { Component } from 'react';
import Select from 'react-select'
import countryList from 'react-select-country-list'
import { Link } from 'react-router-dom'
import '../../styles/all.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronCircleRight } from '@fortawesome/free-solid-svg-icons'
 
class CountrySelector extends Component {
  constructor(props) {
    super(props)
 
    this.options = countryList().getData()
 
    this.state = {
      options: this.options,
      value: null,
      content: null
    }
  }
 
  changeHandler = value => {
    this.setState({ value })  
  }
 
  render() {
    return (
      <div className="selectContainer">
        <Select
        options={this.state.options}
        value={this.state.value}
        onChange={this.changeHandler}
      />
      <div className="paddinged">
        <Link className="goBtn" to={"/destinations/" + (this.state.value ? this.state.value.label : 'all') + "/"} name="GO" style={{fontWeight: 900, color: '#725AC1'}}><FontAwesomeIcon icon={faChevronCircleRight} /></Link>
      </div>
      
      </div>
        
      
    )
  }
}

export default CountrySelector;