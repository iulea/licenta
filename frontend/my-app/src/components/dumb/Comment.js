import React from 'react';
import '../../styles/all.css';
import Rating from '@material-ui/lab/Rating';
import { Link } from "react-router-dom";

class Destination extends React.Component{
    constructor(props) {
        super(props);
      
        this.state = {
          name: props.name,
          stars: props.stars,
          dateAdded: props.dateAdded,
          comment: props.comment,
        };
      }

      render() {
        return <div className="comment-container">
              <div className="comment-half">
                <h3>{this.state.name}</h3>
                {this.state.dateAdded}
              </div>
              <div className="comment-half">
                <Rating name="half-rating" value={this.state.stars} precision={0.1} readOnly/>
                {this.state.comment}
              </div>
            </div>        
      }
}

export default Destination