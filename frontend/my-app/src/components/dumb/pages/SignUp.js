import React, { Component } from 'react';
import {observer} from "mobx-react";
import {extendObservable} from "mobx";
import {Redirect} from "react-router-dom";
import '../../../styles/all.css'
import Header from '../Header';
import { withRouter } from "react-router-dom";

export default withRouter(observer(
class SignUp extends Component
{
    constructor(props) {
        super(props);
        extendObservable(this, {
            username: '',
            email: '',
            password: '',
            confirmPassword: '',
            firstName: '',
            lastName: '',
            errorMessage: "",
        });
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        const {name, value} = e.target;
        this[name] = value;
    };


    handleSubmit = e => {
        e.preventDefault();
        this.errorMessage = "";
        const {username, firstName, lastName, email, password, confirmPassword} = this;
        if(username === '' || firstName === '' || lastName === '' || email === '' || password === '' || confirmPassword === '') {
            this.errorMessage = "Fields cannot be empty!" ;
            return
        }
        if(password !== confirmPassword) {
            this.errorMessage = "Passwords do not match!";
            return
        }
        if(!email.includes("@")) {
            this.errorMessage = "Invalid email address!";
            return
        }
        const obj = {
            "username": username,
            "email": email,
            "firstName": firstName,
            "lastName": lastName,
            "password": password
        }
        fetch("http://127.0.0.1:8000/createUser/", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        }).then(response => {
            if(response.status !== 200 && response.status !== 201){
                response.json().then(text => this.errorMessage = text)
                this.setState(null)
                return null;
            }
            return response.json();
        }).then(result => {
            if(result != null){
                this.errorMessage = "";
               this.props.history.push("/login");
            }
        })
    };

    render(){
        const {username, firstName, lastName, email, password, confirmPassword} = this;
        let err;
        if(typeof this.errorMessage == "object") {
            const arr = JSON.parse(JSON.stringify(this.errorMessage))
            console.log(arr)
            err = Object.keys(arr).map(error => <div>{arr[error]}</div>)
        }

        return <div className="main-div2">
            <Header/>
            <div className="form-content margin-top">
                <form className="form-fields">
                    <input type="file" ref={input => this.inputElement = input} accept="image/*" name="pic"/>
                    <div className="imgWrap"><div className="whiteWrap"><div onClick={() => {this.inputElement.click();}} id="selectImg"></div></div></div>
                    <input required className="formField-input" placeholder="username" name="username" value={username} onChange={this.handleChange} />
                    <input required className="formField-input" placeholder="email" name="email" value={email} onChange={this.handleChange} />
                    <input required className="formField-input" placeholder="first name" name="firstName" value={firstName} onChange={this.handleChange} />
                    <input required className="formField-input" placeholder="last name"  name="lastName" value={lastName} onChange={this.handleChange} />
                    <input required type="password" id="password" className="formField-input" placeholder="password" name="password" value={password} onChange={this.handleChange} />
                    <input required type="password" id="confirmPpassword" className="formField-input" placeholder="repeat password"  name="confirmPassword" value={confirmPassword} onChange={this.handleChange} />
                    <div className="errors">{typeof this.errorMessage == "object" ? err : this.errorMessage}</div>
                    <button className="pretty-btn" onClick={this.handleSubmit}>Sign up</button>
                </form>
            </div>
        </div>
        
        
    }
}))