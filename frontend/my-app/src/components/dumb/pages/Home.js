import React, { Component } from 'react';
import CountrySelector from '../Map';
import '../../../styles/all.css'
import Header from '../Header';

class Home extends Component
{

    render()
    {
        return (
            <div className="main-div">
                <Header/>
                <div className="main-container">
                    <h1>Where to...?</h1>
                    <div className="country-select">
                        <CountrySelector/>
                    </div>
                </div>
            </div>
        );
    }

}

export default Home