import React from 'react';
import { observer } from "mobx-react";
import {extendObservable} from "mobx";
import storeDestinations from '../../smart/getDestinations';
import storeSuggestions from '../../smart/getSuggestions';
import Header from '../Header';
import Destination from '../Destination';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import {Link} from "react-router-dom";

const DestinationsList = observer(class DestinationsList extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            city: -1,
            citiesList: [],
            category: -1,
            suggest: 0
        }
        extendObservable(this, {
            search: ''
        });
        this.handleSearch = this.handleSearch.bind(this);
        this.handleCityChange = this.handleCityChange.bind(this);
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
        this.removeFilters = this.removeFilters.bind(this);
        this.suggest = this.suggest.bind(this);
    }

    handleSearch = e => {
        const {name, value} = e.target;
        this[name] = value;
    };

    componentDidMount() {
        if (storeDestinations.loaded === false) {
            if(this.props.match.params.country != 'all') {
                storeDestinations.country = this.props.match.params.country;
            }
            storeDestinations.loadDestinations();
        }
        if (localStorage.getItem('Access-Token') && storeSuggestions.loaded === false) {
            if(this.props.match.params.country != 'all') {
                storeSuggestions.country = this.props.match.params.country;
            }
            storeSuggestions.loadDestinations();
        }
        this.setState();
    }

    suggest() {
        this.setState({suggest: !this.state.suggest})
    }

    componentWillUnmount() {
        storeDestinations.unload();
    }

    handleCityChange(event) {
        this.state.city = event.target.value;
        this.setState(this.state)
    };

    handleCategoryChange(event) {
        this.state.category = event.target.value;
        this.setState(this.state)
    };

    removeFilters() {
        this.state.city = -1;
        this.state.category = -1;
        this.search = '';
        this.setState(this.state)
    }

    render(){
        const {search} = this;
        let {destinations} = JSON.parse(JSON.stringify(storeDestinations));
        if(this.state.suggest) {
            destinations = JSON.parse(JSON.stringify(storeSuggestions.destinations));
        }

        let listOfDestinations;
        let listOfCities;
        if(destinations) {
            destinations.map(destination => ((!this.state.citiesList.includes(destination.city)) ? this.state.citiesList.push(destination.city): ''));
            listOfCities = this.state.citiesList.map(city => <MenuItem value={city}>{city}</MenuItem>);
            listOfDestinations = destinations.map(destination => 
                ((this.state.city == -1|| this.state.city == destination.city) && (this.state.category == -1|| this.state.category == destination.category)) ? ((destination.name.toUpperCase().includes(search.toUpperCase()) || search == '') ? 
                    <Destination key={destination.id} name={destination.name} id={destination.id} country={destination.country} city={destination.city} category={destination.category} stars={destination.stars}/> : '') : ''
            );
            console.log(listOfDestinations)
        }         
        return (
            <div className="main-div">
            <Header/>
            <div className="main-container">
                <div className="filters">
                    <Select
                        value={this.state.city}
                        onChange={this.handleCityChange}
                        >
                        <MenuItem value={-1}>All cities</MenuItem>
                        {listOfCities}
                    </Select>
                    <Select
                        value={this.state.category}
                        onChange={this.handleCategoryChange}
                        >
                        <MenuItem value={-1}>All categories</MenuItem>
                        <MenuItem value={1}>Culture</MenuItem>
                        <MenuItem value={2}>Entertainment</MenuItem>
                        <MenuItem value={3}>Food</MenuItem>
                        <MenuItem value={4}>Bars</MenuItem>
                        <MenuItem value={5}>Services</MenuItem>
                        <MenuItem value={6}>Shopping</MenuItem>
                        <MenuItem value={7}>Sport</MenuItem>
                        <MenuItem value={8}>Study</MenuItem>
                        <MenuItem value={9}>Other</MenuItem>
                    </Select>
                    <TextField placeholder="search" name="search" value={search} onChange={this.handleSearch} />
                    <button className="pretty-btn" style={{width: 160}} onClick={this.removeFilters}>Remove filters</button>
                    {localStorage.getItem('Access-Token') ? !this.state.suggest ? <button className="pretty-btn" style={{width: 260}} onClick={this.suggest}>Tell me what to visit here!</button> :
                        <button className="pretty-btn" style={{width: 260}} onClick={this.suggest}>Show all</button> : <div></div>}
                </div>
                {this.state.suggest ? <div><span><strong>Hint:</strong> the more you review destinations, you will receive more suggestions and more accurate ones</span></div> : <div></div>}
                <div className="listDestinations">
                        {listOfDestinations}
                        {localStorage.getItem('Access-Token') && ! this.state.suggest ? <Link to={"/addDestination"}><button className="pretty-btn" style={{width: 200}}>Couldn't find what you were looking for? Help improve this list by adding more destinations</button></Link> : <div/>}
                </div>
            </div>
        </div>

        );}
});

export default DestinationsList