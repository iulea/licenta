import React, { Component } from 'react';
import {observer} from "mobx-react";
import {extendObservable} from "mobx";
import {Redirect} from "react-router-dom";
import '../../../styles/all.css';
import Header from '../Header';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const AddDestination = observer(
class AddDestination extends Component
{
    constructor(props) {
        super(props);
        extendObservable(this, {
            name: '',
            country: '',
            city: '',
            category: -1,
            errorMessage: "",
            redirect: false
        });
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        const {name, value} = e.target;
        this[name] = value;
    };


    handleSubmit = e => {
        e.preventDefault();
        this.errorMessage = "";
        const {name, country, city, category} = this;
        if(name === '' || country === '' || city === '' || category === '') {
            this.errorMessage = "Fields cannot be empty!" ;
            return
        }

        const obj = {
            "name": name,
            "country": country,
            "city": category,
            "category": category
        }
        fetch("http://127.0.0.1:8000/api/destinations/", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        }).then(response => {
            if(response.status !== 200 && response.status !== 201){
                this.errorMessage = "Something went wrong :(";
                return null;
            }
            return response.json();
        }).then(result => {
            if(result != null){
                this.errorMessage = "";
                this.redirect = true;
            }
        })
    };

    render(){
        const {name, country, city, category} = this;
        let err;
        if(typeof this.errorMessage == "object") {
            const arr = JSON.parse(JSON.stringify(this.errorMessage))
            console.log(arr)
            err = Object.keys(arr).map(error => <div>{arr[error]}</div>)
        }

        return <div className="main-div2">
            <Header/>
            <div className="form-content margin-top">
                <form className="form-fields">
                    <div className="large-div"><h1>Add destination</h1></div>
                    <input required className="formField-input" placeholder="destination name" name="name" value={name} onChange={this.handleChange} />
                    <input required className="formField-input" placeholder="country" name="country" value={country} onChange={this.handleChange} />
                    <input required className="formField-input" placeholder="city" name="city" value={city} onChange={this.handleChange} />
                    <Select 
                        name="category"
                        value={category}
                        onChange={this.handleChange}
                        style={{width: 100+ "%"}}
                        >
                        <MenuItem value={-1}>All categories</MenuItem>
                        <MenuItem value={1}>Culture</MenuItem>
                        <MenuItem value={2}>Entertainment</MenuItem>
                        <MenuItem value={3}>Food</MenuItem>
                        <MenuItem value={4}>Bars</MenuItem>
                        <MenuItem value={5}>Services</MenuItem>
                        <MenuItem value={6}>Shopping</MenuItem>
                        <MenuItem value={7}>Sport</MenuItem>
                        <MenuItem value={8}>Study</MenuItem>
                        <MenuItem value={9}>Other</MenuItem>
                    </Select>
                    <div className="errors">{this.errorMessage}</div>
                    <button className="pretty-btn" onClick={this.handleSubmit}>Submit</button>
                    {this.redirect? <React.Fragment>
                            <Redirect to="/"/>
                            {window.location.reload()}
                        </React.Fragment> : ""}
                </form>
            </div>
        </div>
        
        
    }
})

export default AddDestination