import React, { Component } from 'react';
import {observer} from "mobx-react";
import {extendObservable} from "mobx";
import {Redirect} from "react-router-dom";
import '../../../styles/all.css';
import Header from '../Header';
import Rating from '@material-ui/lab/Rating';
import Tooltip from '@material-ui/core/Tooltip';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import IconButton from '@material-ui/core/IconButton';
import { withRouter } from "react-router-dom";

const Review = withRouter(observer(
class Review extends Component
{
    constructor(props) {
        super(props);
        extendObservable(this, {
            cost: 0,
            time: 0,
            family: 0,
            interactive: 0,
            newppl: 0,
            loud: 0,
            overall: 0,
            comment: '',
            considered: false,
        });
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeInt = this.handleChangeInt.bind(this);
        this.handleChangeCheck = this.handleChangeCheck.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        const {name, value} = e.target;
        this[name] = value;
    }

    handleChangeInt = e => {
        const {name, value} = e.target;
        this[name] = parseInt(value);
    };

    handleChangeCheck = e => {
        const {name} = e.target;
        e.checked = !e.checked;
        this[name] = e.checked;
    }


    handleSubmit = e => {
        e.preventDefault();
        let {cost, time, family, interactive, newppl, loud, overall, comment, considered} = this;
        if(!comment) {
            comment = " - "
        }

        const obj = {
            "destination": this.props.match.params.id,
            "cost": cost,
            "time": time,
            "family": family,
            "interactive": interactive,
            "newppl": newppl,
            "loud": loud,
            "overall": overall,
            "comment": comment,
            "considered": considered
        }
        fetch("http://127.0.0.1:8000/addReview/", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('Access-Token')
            },
            body: JSON.stringify(obj)
        }).then(response => {
            if(response.status !== 201){
                alert("Something went wrong")
                return null;
            }
            return response.json();
        }).then(result => {
            if(result != null){
                
               this.props.history.push("/destinations/all");
            }
        })
    };

    render(){
        const {cost, time, family, interactive, newppl, loud, overall, comment, considered} = this;
        return <div className="main-div">
            <Header/>
            <div className="form-content margin-top">
                <form className="form-fields">
                    <div className="review-input">
                        <div className="with-help-text">
                            <h3>Cost</h3>           
                            <Tooltip title="The ammount of money you spent. (1 - free, 5 - destination required a lot of money)">
                                <IconButton aria-label="delete">
                                <FontAwesomeIcon icon={faQuestionCircle} />
                                </IconButton>
                            </Tooltip>
                        </div>
                        <Rating name="cost" value={cost} precision={1} onChange={this.handleChangeInt}/></div>
                    <div className="review-input">
                        <div className="with-help-text">
                            <h3>Time needed</h3>           
                            <Tooltip title="How much time spent the destination required. (1 - less the an hour, 5 - more than 4 hours)">
                                <IconButton aria-label="delete">
                                <FontAwesomeIcon icon={faQuestionCircle} />
                                </IconButton>
                            </Tooltip>
                        </div>
                        <Rating name="time" value={time} precision={1} onChange={this.handleChangeInt}/></div>
                    <div className="review-input">
                        <div className="with-help-text">
                            <h3>Family friendly</h3>           
                            <Tooltip title="How family friendly the location is. (1 - not at all, 5 - perfect for families)">
                                <IconButton aria-label="delete">
                                <FontAwesomeIcon icon={faQuestionCircle} />
                                </IconButton>
                            </Tooltip>
                        </div>
                        <Rating name="family" value={family} precision={1} onChange={this.handleChangeInt}/></div>
                    <div className="review-input">
                        <div className="with-help-text">
                            <h3>Interactive</h3>           
                            <Tooltip title="How interactive the destination is. (1 - not at all, 5 - very interactive)">
                                <IconButton aria-label="delete">
                                <FontAwesomeIcon icon={faQuestionCircle} />
                                </IconButton>
                            </Tooltip>
                        </div>
                        <Rating name="interactive" value={interactive} precision={1} onChange={this.handleChangeInt}/></div>
                    <div className="review-input">
                        <div className="with-help-text">
                            <h3>Opportunity to meet new people</h3>           
                            <Tooltip title="How easy it is to meet and interact with other people. (1 - not at all, 5 - made lots of friends there)">
                                <IconButton aria-label="delete">
                                <FontAwesomeIcon icon={faQuestionCircle} />
                                </IconButton>
                            </Tooltip>
                        </div>
                        <Rating name="newppl" value={newppl} precision={1} onChange={this.handleChangeInt}/></div>
                    <div className="review-input">
                        <div className="with-help-text">
                            <h3>Loud</h3>           
                            <Tooltip title="How noisy the destination is. (1 - classroom when teacher asks something, 5 - metal concert)">
                                <IconButton aria-label="delete">
                                <FontAwesomeIcon icon={faQuestionCircle} />
                                </IconButton>
                            </Tooltip>
                        </div>
                        <Rating name="loud" value={loud} precision={1} onChange={this.handleChangeInt}/></div>
                    <div className="review-input"><h3>Overall</h3><Rating name="overall" value={overall} precision={1} onChange={this.handleChangeInt}/></div>
                    <div className="review-input">
                        <div className="with-help-text">
                            <h3>Yes, I wish to be recommended more like this</h3>           
                            <Tooltip title="If you leave this unchecked, this destination won't be considered when suggesting new destinations. Recommended to check if you enjoyed it.">
                                <IconButton aria-label="delete">
                                <FontAwesomeIcon icon={faQuestionCircle} />
                                </IconButton>
                            </Tooltip>
                        </div>
                    <input type="checkbox" name="considered" onChange={this.handleChangeCheck}/></div>
                    <textarea rows="5" className="formField-input" placeholder="comment"  name="comment" value={comment} onChange={this.handleChange} /> 
                    <button className="pretty-btn" onClick={this.handleSubmit}>Submit review</button>
                </form>
            </div>
        </div>        
    }
}))

export default Review