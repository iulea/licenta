import React, { Component } from 'react';
import {observer} from "mobx-react";
import {extendObservable} from "mobx";
import {Link, Redirect} from "react-router-dom";
import '../../../styles/all.css'
import Header from '../Header';

export default observer(
class Login extends Component
{
    constructor(props) {
        super(props);
        extendObservable(this, {
            username: '',
            password: '',
            redirect: false,
            errorMessage: ''
        });
        this.handleChange = this.handleChange.bind(this);
        this.login = this.login.bind(this);
    }

    handleChange = e => {
        const {name, value} = e.target;
        this[name] = value;
    };


    login = (e) => {
        e.preventDefault();
        const {username, password} = this;
        const obj = {
            "username": username,
            "password": password
        }
        fetch("http://127.0.0.1:8000/login/", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        }).then(response => {
            if(response.status !== 200){
                this.errorMessage = "Username or password is incorrect";
                return null;
            }
            return response.json();
        }).then(result => {
            if(result != null){
                localStorage.setItem("Access-Token", result.token);
                this.errorMessage = "";
                this.redirect = true;
            }
        })

    };

    render(){
        const {username, password} = this;
        return <div className="main-div">
            <Header/>
            <div className="form-content">
                <form className="form-fields">
                    <div className="large-div"><h1>hello!</h1></div>
                    <input required id="username" className="formField-input" placeholder="username" name="username" value={username} onChange={this.handleChange} />
                    <input required type="password" id="password" className="formField-input" placeholder="password"  name="password" value={password} onChange={this.handleChange} />
                    <div className="errors">{this.errorMessage}</div>
                    <button className="pretty-btn" onClick={this.login}>Log in</button>
                    <Link className="App-link" to="/test"><b>Forgot your password?</b></Link><br/>
                    <Link className="App-link" to="/signup"><b>Don't have an account yet? Sign up here!</b></Link>
                    {this.redirect? <React.Fragment>
                            <Redirect to="/"/>
                            {window.location.reload()}
                        </React.Fragment> : ""}

                </form>
            </div>
        </div>        
    }
})