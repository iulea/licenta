import React from 'react';
import { observer } from "mobx-react";
import Header from '../Header';
import Rating from '@material-ui/lab/Rating';
import Comment from './../Comment';
import {Link} from "react-router-dom";

const DestinationPage = observer(class DestinationsList extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            destination: null,
            reviews: null
        }
    }

    handleSearch = e => {
        const {name, value} = e.target;
        this[name] = value;
    };

    componentDidMount() {
        fetch(`http://127.0.0.1:8000/api/destinations/` + this.props.match.params.id)
            .then(data => data.json())
            .then(response => {
                this.setState({destination: response})
                fetch(`http://127.0.0.1:8000/api/reviews/?destination=` + this.state.destination.id)
                    .then(data => data.json())
                    .then(response => {
                        this.setState({reviews: response})
                    })
            })
    }

    updateStars(stars) {
        const obj = {
            "stars": stars
        }
        fetch(`http://127.0.0.1:8000/api/destinations/` + this.props.match.params.id + '/', {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
    }

    render(){
        const destination = this.state.destination;
        const categories = {1: "culture", 2:"entertainment", 3: "food", 4: "bars", 5: "services", 6: "shopping", 7: "sport", 8: "study", 9:"other"}
    
        if(this.state.destination) {
            let comments = null
            let stars = 0
            if(this.state.reviews) {
                comments = this.state.reviews.map(review => <Comment name={review.user['username']} dateAdded={review.dateAdded} comment={review.comment} stars={review.overall}/>)
                stars = 0;
                this.state.reviews.forEach(review => stars += review.overall)
                stars = stars / this.state.reviews.length
                if(stars != destination.stars) {
                    this.updateStars(stars)
                }
            }

            return (
            <div className="main-div">
            <Header/>
            <div className="main-container">
                <div className="details-container">
                    <h1>{destination.name}</h1>
                    <Rating name="half-rating" value={stars} precision={0.1} readOnly/><br/>
                    <span>{destination.country}: <strong>{destination.city}</strong></span><br/>
                    <span>Category: <strong>{categories[destination.category]}</strong></span>
                    <br/>
                    <div className="comments-section">{comments && comments.length > 0 ? <h2>Comments:</h2>  : ""}<Link to={"/review/" + this.props.match.params.id}><button className="pretty-btn" style={{width: 200}}>Review this destination</button></Link></div>
                    {this.state.reviews ? comments : ""}
                </div>                
                
            </div>
        </div>

        )
        } else {
            return (
            <div className="main-div">
                <Header/>
                <div className="main-container">                
                    <div className="listDestinations">
                        No destination found with that id!
                    </div>
                </div>
            </div>
            )
        }
        ;}
});

export default DestinationPage