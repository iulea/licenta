import React from 'react'
import '../../styles/header.css'
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {Link} from "react-router-dom";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#FFAF87',
        },
        secondary: {
            main: '#8D86C9',
        },
        inherit: {
            main: '#80ced6',
        },
    },
});

class Header extends React.Component
{
    constructor(props) {
        super(props);
    }


    render() {
        const {actionLogout} = this.props;

        function handleButtonClicked() {
            localStorage.clear();
            sessionStorage.clear();
            window.location.reload();

        }

        // const {btns} = this.props;
        return (
            <MuiThemeProvider theme={theme}>
                <div className="root">
                    <AppBar position="static" color="primary">
                        <Toolbar>
                          <Link to='/'>
                            <div className="homeText">
                              <div className="planes"></div>
                              <Button color="inherit" className="menuButton">HOME</Button>
                            </div>
                          </Link>                     
                            <Typography variant="h6" className="grow"></Typography>
                            <div style={{align: "right"}} className="homeText">
                                {localStorage.getItem("Access-Token") || sessionStorage.getItem('Access-Token') ? <Button color="inherit" className="menu-button" onClick={handleButtonClicked}>LOG OUT</Button> 
                                    : <Link to='/login'>
                                        <div className="home-text">
                                            <Button color="inherit" className="menu-button">Sign in</Button>
                                        </div>
                                    </Link>
                                }
                            </div>
                        </Toolbar>
                    </AppBar>

                </div>
            </MuiThemeProvider>
        )
    }
}

export default Header;